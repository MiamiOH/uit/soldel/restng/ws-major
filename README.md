# Major RESTful Web Service

## Description

ws-major is implemented under RESTng framework and used to provide information of major.
It was originally written in RestServer.

## API Documentation

API documentation can be found on swagger page: <ws_url>/api/swagger-ui/#/areaOfStudy

## Local Development Setup

1. pull down latest source code from this repository
2. install composer dependencies: `composer update`
3. make sure `pike` has installed as an addon of `restng`
4. make symlink in the `restng` folder (make sure environment variable `$RESTNG_HOME` is set)

```bash
ln -s <path_to_project>/src/RESTconfig $RESTNG_HOME/RESTconfig/areaOfStudy
ln -s <path_to_project>/src/Service $RESTNG_HOME/Service/AreaOfStudy
```

## Testing

### Unit Testing

Unit test cases in this project is written using PHPUnit. 

`phpunit` should pass without any error message before and after making any change. Code coverage report will be
automatically generated after `phpunit` being ran and put into `test/coverage` folder.