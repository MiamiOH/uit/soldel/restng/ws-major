<?php
/**
 * Author: liaom
 * Date: 4/20/18
 * Time: 3:50 PM
 */

namespace MiamiOH\MajorWebService\Tests\Unit;

use MiamiOH\MajorWebService\Services\MajorService;
use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Service\ViewMajorService;
use MiamiOH\Pike\Domain\Collection\MajorCollection;
use MiamiOH\Pike\Domain\Model\Major;
use MiamiOH\Pike\Domain\ValueObject\MajorCode;
use MiamiOH\Pike\Exception\Exception;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Class MajorServiceTest
 */
class MajorServiceTest extends \MiamiOH\RESTng\Testing\TestCase
{
    /**
     * @var MajorService
     */
    private $majorService;
    /**
     * @var MockObject
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var MockObject
     */
    private $viewMajorService;

    /**
     * @var MockObject
     */
    private $pike;
    /**
     * @var MockObject
     */
    private $pikeServiceFactory;

    /**
     * @var Major
     */
    private $major1;
    /**
     * @var Major
     */
    private $major2;
    /**
     * @var Major
     */
    private $major3;

    /**
     * @var array
     */
    private $majorPayload1;
    /**
     * @var array
     */
    private $majorPayload2;
    /**
     * @var array
     */
    private $majorPayload3;

    private function pass()
    {
        $this->assertTrue(true);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();
        $this->majorService = new MajorService();

        $this->viewMajorService = $this->getMockBuilder(ViewMajorService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->pike = $this->createMock(AppMapper::class);
        $this->pike->method('getViewMajorService')->willReturn($this->viewMajorService);

        $this->pikeServiceFactory = $this->createMock(PikeServiceFactory::class);
        $this->pikeServiceFactory->method('getEloquentPikeServiceMapper')->willReturn($this->pike);

        $this->majorService->setPikeServiceFactory($this->pikeServiceFactory);
        $this->majorService->setRequest($this->request);

        $this->major1 = new Major(new MajorCode('APET'), 'askdldk skdlsk s');
        $this->major2 = new Major(new MajorCode('0000'), 'asd sd sdafasds ');
        $this->major3 = new Major(new MajorCode('AABB'), 'asdfas s ad s d');

        $this->majorPayload1 = $this->majorToMajorPayload($this->major1);
        $this->majorPayload2 = $this->majorToMajorPayload($this->major2);
        $this->majorPayload3 = $this->majorToMajorPayload($this->major3);
    }

    private function majorToMajorPayload(Major $major): array
    {
        return [
            'code' => $major->getCode(),
            'description' => $major->getDescription()
        ];
    }

    public function testCanGetMajorByCode()
    {
        $this->request->method('getResourceParam')->willReturn($this->major1->getCode());
        $this->viewMajorService->method('getByCode')->willReturn($this->major1);

        $response = $this->majorService->getMajorByCode();

        $this->assertEquals($this->majorPayload1, $response->getPayload());
    }

    public function testMajorCodeShouldBeCastToUppercaseAndTrimWhiteSpace()
    {
        $code = '    apet    ';
        $this->request->method('getResourceParam')->willReturn($code);
        $this->viewMajorService
            ->method('getByCode')
            ->with($this->major1->getCode())
            ->willReturn($this->major1);

        $response = $this->majorService->getMajorByCode();

        $this->assertEquals($this->majorPayload1, $response->getPayload());
    }

    public function testInvalidMajorCodeWillCauseException()
    {
        $this->request->method('getResourceParam')->willReturn('asdfas');
        $this->viewMajorService->method('getByCode')->willThrowException(new Exception());

        $response = $this->majorService->getMajorByCode();
        $this->assertEquals(App::API_NOTFOUND, $response->getStatus());

    }

    public function testCanGetAllMajors()
    {
        $this->request->method('getOptions')->willReturn([]);
        $this->viewMajorService->method('getAll')->willReturn(new MajorCollection([
            $this->major1,
            $this->major2
        ]));

        $response = $this->majorService->getMajorCollection();

        $this->assertEquals([$this->majorPayload1, $this->majorPayload2],
            $response->getPayload());
    }

    public function testMajorCodesShouldBeTrimAndCaseToUppercase()
    {
        $codes = ['apet  ', '  sdf '];
        $this->request->method('getOptions')->willReturn(['codes' => $codes]);
        $this->viewMajorService
            ->method('getMajorCollectionByCodes')
            ->with(['APET', 'SDF'])
            ->willReturn(new MajorCollection([$this->major1, $this->major2]));

        $response = $this->majorService->getMajorCollection();

        $this->assertEquals([$this->majorPayload1, $this->majorPayload2],
            $response->getPayload());
    }
}