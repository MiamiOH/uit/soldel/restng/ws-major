<?php
/**
 * Author: liaom
 * Date: 7/23/18
 * Time: 3:21 PM
 */

namespace MiamiOH\MajorWebService\Tests\Feature;

use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Service\ViewMajorService;
use MiamiOH\Pike\Domain\Collection\MajorCollection;
use MiamiOH\Pike\Domain\Model\Major;
use MiamiOH\Pike\Domain\ValueObject\MajorCode;
use MiamiOH\Pike\Exception\MajorNotFoundException;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class MajorServiceTest extends TestCase {
    /**
     * @var MockObject
     */
    private $viewMajorService;

    protected function setUp()
    {
        parent::setUp();
        $pikeServiceFactory = $this->createMock(PikeServiceFactory::class);
        $this->viewMajorService = $this->createMock(ViewMajorService::class);
        $pike = $this->createMock(AppMapper::class);
        $pike->method('getViewMajorService')->willReturn($this->viewMajorService);
        $pikeServiceFactory->method('getEloquentPikeServiceMapper')->willReturn($pike);

        $this->app->useService([
            'name' => 'PikeServiceFactory',
            'object' => $pikeServiceFactory,
            'description' => 'Mocked Pike Service Factory'
        ]);
    }

    public function testGetMajorByCode() {
        $major = $this->mockMajor('0000', 'something');

        $this->viewMajorService
            ->method('getByCode')
            ->with('0000')
            ->willReturn($major);

        $response = $this->getJson('/areaOfStudy/v1/major/0000');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'code' => '0000',
                'description' => 'something'
            ]
        ]);
    }

    public function testMajorByCodeNotFound() {
        $this->viewMajorService
            ->method('getByCode')
            ->with('0000')
            ->willThrowException(new MajorNotFoundException());

        $response = $this->getJson('/areaOfStudy/v1/major/0000');

        $response->assertStatus(App::API_NOTFOUND);
    }

    public function testGetAllMajors() {
        $major1 = $this->mockMajor('0000', 'something');
        $major2 = $this->mockMajor('APET', 'something 2');
        $majorCollection = new MajorCollection([$major1, $major2]);

        $this->viewMajorService
            ->method('getAll')
            ->willReturn($majorCollection);

        $response = $this->getJson('/areaOfStudy/v1/major');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                [
                    'code' => '0000',
                    'description' => 'something'
                ],
                [
                    'code' => 'APET',
                    'description' => 'something 2'
                ]
            ]
        ]);
    }

    public function testGetMultipleMajorByCodes() {
        $major1 = $this->mockMajor('0000', 'something');
        $major2 = $this->mockMajor('APET', 'something 2');
        $majorCollection = new MajorCollection([$major1, $major2]);

        $this->viewMajorService
            ->method('getMajorCollectionByCodes')
            ->with(['0000', 'APET'])
            ->willReturn($majorCollection);

        $response = $this->getJson('/areaOfStudy/v1/major?codes=0000,APET');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                [
                    'code' => '0000',
                    'description' => 'something'
                ],
                [
                    'code' => 'APET',
                    'description' => 'something 2'
                ]
            ]
        ]);
    }

    private function mockMajor(
        string $code,
        string $description
    ): MockObject {
        $major = $this->createMock(Major::class);
        $major->method('getCode')->willReturn(new MajorCode($code));
        $major->method('getDescription')->willReturn($description);
        return $major;
    }
}