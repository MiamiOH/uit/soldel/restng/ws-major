<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 9:34 PM
 */

namespace MiamiOH\MajorWebService\Resources;


use MiamiOH\RESTng\App;

class MajorResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{

    private $serviceName = 'AreaOfStudy';
    private $tag = "areaOfStudy";
    private $resourceRoot = "areaofstudy.major";
    private $patternRoot = "/areaOfStudy/v1";
    private $classPath = 'MiamiOH\MajorWebService\Services\MajorService';

    public function registerDefinitions(): void
    {
        $this->addTag([
            'name' => 'areaOfStudy',
            'description' => 'Banner Area of Study Web Services'
        ]);

        $this->addDefinition([
            'name' => 'Major',
            'type' => 'object',
            'properties' => [
                'code' => [
                    'type' => 'string',
                ],
                'description' => [
                    'type' => 'string',
                ]
            ]
        ]);

        $this->addDefinition([
            'name' => 'Major.Exception',
            'type' => 'object',
            'properties' => []
        ]);

        $this->addDefinition([
            'name' => 'Major.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Major'
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->serviceName,
            'class' => $this->classPath,
            'description' => 'Banner Area of Study Web Services',
            'set' => [
                'pikeServiceFactory' => [
                    'type' => 'service',
                    'name' => 'PikeServiceFactory'
                ],
            ],
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
                'action' => 'read',
                'name' => $this->resourceRoot . '.byCode.get',
                'description' => 'get one major by code',
                'pattern' => $this->patternRoot . '/major/:code',
                'service' => $this->serviceName,
                'method' => 'getMajorByCode',
                'isPageable' => false,
                'tags' => [$this->tag],
                'params' => [
                    'code' => ['description' => 'major code']
                ],
                'options' => [
                ],
                'middleware' => [],
                'responses' => [
                    App::API_OK => [
                        'description' => 'information of a major',
                        'returns' => [
                            'type' => 'model',
                            '$ref' => '#/definitions/Major'
                        ],
                    ],
                    App::API_NOTFOUND => [
                        'description' => 'Inquiry with invalid major code ',
                        'returns' => [
                            'type' => 'object',
                            '$ref' => '#/definitions/Major.Exception'
                        ],
                    ]
                ]
            ]
        );

        $this->addResource([
                'action' => 'read',
                'name' => $this->resourceRoot . '.all.get',
                'description' => 'get a collection of majors',
                'pattern' => $this->patternRoot . '/major',
                'service' => $this->serviceName,
                'method' => 'getMajorCollection',
                'isPageable' => false,
                'tags' => [$this->tag],
                'params' => [],
                'options' => [
                    'codes' => [
                        'description' => 'major codes',
                        'type' => 'list'
                    ]
                ],
                'middleware' => [],
                'responses' => [
                    App::API_OK => [
                        'description' => 'read a collection of all majors',
                        'returns' => [
                            'type' => 'array',
                            '$ref' => '#/definitions/Major.Collection'
                        ],
                    ],

                ]
            ]
        );
    }

    public function registerOrmConnections(): void
    {

    }
}