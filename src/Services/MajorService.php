<?php

namespace MiamiOH\MajorWebService\Services;

use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Service\ViewMajorService;
use MiamiOH\Pike\Domain\Collection\MajorCollection;
use MiamiOH\Pike\Domain\Model\Major;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;

class MajorService extends Service
{
    /**
     * @var string
     */
    private $datasource_name = 'MUWS_GEN_PROD';
    /**
     * @var Request
     */
    private $request;

    /**
     * @var array
     */
    private $options;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var AppMapper
     */
    private $pike;

    /**
     * @var ViewMajorService
     */
    private $viewMajorService;

    private function setup()
    {
        $this->request = $this->getRequest();
        $this->response = $this->getResponse();
        $this->options = $this->request->getOptions();
    }

    public function setPikeServiceFactory(PikeServiceFactory $pikeServiceFactory)
    {
        $this->pike = $pikeServiceFactory->getEloquentPikeServiceMapper($this->datasource_name);
        $this->viewMajorService = $this->pike->getViewMajorService();
    }

    public function getMajorByCode()
    {
        $this->setup();

        $code = $this->request->getResourceParam('code');
        $major = null;
        try {
            $major = $this->viewMajorService->getByCode(strtoupper(trim($code)));
            $this->response->setStatus(App::API_OK);
            $this->addToPayload($this->majorToArray($major));
        } catch (\Exception $e) {
            $this->response->setStatus(App::API_NOTFOUND);
        }

        return $this->response;
    }

    public function getMajorCollection()
    {
        $this->setup();

        $majorCollection = null;

        if (isset($this->options['codes'])) {
            $codes = $this->options['codes'];
            $codes = array_map(function ($code) {
                return strtoupper(trim($code));
            }, $codes);

            $majorCollection = $this->viewMajorService->getMajorCollectionByCodes($codes);
        } else {
            $majorCollection = $this->viewMajorService->getAll();
        }

        $this->addMajorCollectionToPayload($majorCollection);

        return $this->response;
    }

    private function addToPayload(array $data)
    {
        $payload = $this->response->getPayload();

        $payload = array_merge($payload, $data);

        $this->response->setPayload($payload);
    }

    private function majorToArray(Major $major): array
    {
        return [
            'code' => (string)$major->getCode(),
            'description' => $major->getDescription()
        ];
    }

    private function majorCollectionToArray(MajorCollection $majorCollection): array
    {
        $data = [];

        foreach ($majorCollection as $major) {
            $data[] = $this->majorToArray($major);
        }

        return $data;
    }

    private function addMajorCollectionToPayload(MajorCollection $majorCollection)
    {
        $this->addToPayload($this->majorCollectionToArray($majorCollection));
    }
}